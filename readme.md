## Graph Cleaner Plugin

A Plugin to clean the GroIMP project graph
### General purpose 
This Plugin was mainly designed to clean the GroIMP project graph after the model was edited using the visual delete method. this is necessary because the delete method only removes the visual nodes not the once above like rotations and transformations.
In case of the Exchange graph plugin this can lead to a large overhead on unused nodes.

### Installation
This repository holds compiled releases for the java 8 and java 11 version of GroIMP at https://gitlab.com/timoberlaender/graphcleaner/-/releases. The zip direcotrys can be unpacked and added to the plugin folder of your GroIMP instance. 


For usage in Eclipse the repository can be cloned or downloaded and imported as an existing eclipse project, **If you are using java 8 the file `/src/module-info.java`  must be deleted**  

### Usage
The pugin can be used in any model based on an RGG-root and the clean function can be found in the menu: "edit > clear Graph". This function will return the amount of deleted nodes in the message panel, yet it will not redraw the 2d-graph view.

#### Settings
In the basic settings the plugin removes all edges that are not branches or successors and all leaf nodes that are not of a subclass of ShadedNull.
This can be customized in the "preferences" panel on the section graph cleaner.
This settings are separated in Whitelist and blacklist settings, in the white list part is a list of edges  that should not be deleted and a list of nodes whose subclasses should not be deleted. In the blacklist part a list there is a list with nodes whose subclasses should be deleted.

The type of the edges are defined by there bits, those can be found in the Attribute editor after selecting one kind of the edge in the graph view.

The type of node is defined by its java path for instance: `de.grogra.turtle.M,de.grogra.imp3d.edit.Translate`


### Theory

Basically the program iterates over the tree starting at the rggRoot node, using recursion.

For each Node the script tests if the class of the node is part of the white list and not part of the black list. 
If a node is not part of the whitelist or is part of the balcklist  and the same condition is fulfilled for all nodes below, the node is removed.
Moreover while iteration all edges that are not part of the whitelist well be removed as well.

