/**
 * 
 */
/**
 * @author tim
 *
 */
module GraphCleaner {
	requires rgg;
	requires platform.core;
	requires platform.swing;
	requires platform;
	requires utilities;
	requires graph;
	requires xl.impl;
	requires xl;
	requires imp;
	requires imp2d;
	requires imp3d;
	
}